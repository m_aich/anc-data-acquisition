# ANC Data Acquisition

This project implements the MRI data acquisition workflow for the Austrian NeuroCloud project. The [workflow's documentation is under development](https://gitlab.com/ccns/neurocog/neurodataops/anc/documentation/data-ccns-documentation/-/tree/main/acquisition/mri).

The workflow is implemented using [Nextflow](https://www.nextflow.io/).

## Basic concepts

The workflow converts data in DICOM format exported from MRI scanner into [BIDS format](https://bids-specification.readthedocs.io/en/stable/).

Currently, the workflow expects the DICOM files be in a TAR archive with a specific name. See the beginning of `./ANC_data_acquisition.nf` file for details. This is specific to Salzburg and is planned to be extended to other sites or more general usage. 

The BIDS conversion is performed using [BIDScoin](https://bidscoin.readthedocs.io/en/stable/) which further depends on [dcm2niix converter](https://github.com/rordenlab/dcm2niix).

The workflow is designed to be used on single subject data and push the converted data into a GitLab repository via a merge request. The GitLab part can be disabled and the workflow can be used for local conversion.

### GitLab specific assumptions

When subject data is pushed to GitLab, the following things happen:
- a new **branch** is created with name `sub-<subid>_ses-<sesid>`
- a new **merge request** is created from branch `sub-<subid>_ses-<sesid>` to `main` branch
- for each data file a new **file** is created in the GitLab project
- all files are pushed with a single commit

If any of these artifacts already exists, the acquisition pipeline **will fail** and the error may not be reported to the user. If the data acquisition procedure is followed, and [interrupted acquisitions](https://gitlab.com/ccns/neurocog/neurodataops/anc/documentation/data-ccns-documentation/-/blob/main/format/datatypes/mri-conversion.md#interrupted-acquisitions) are correctly handled, the pipeline should not fail.

Guidelines, details, and correct BIDScoin mapping are [under development](https://gitlab.com/ccns/neurocog/neurodataops/anc/documentation/data-ccns-documentation/-/issues/26).

Here are some rules:
- Do not export corrupted scans from the scanner.
- If the session was interrupted and the subject had to finish another day, add a suffix to the name of the second part of the session, e.g., `sub-12_ses-2part2`. You will need to rename files in the resulting merge request.

## Workflow overview

```mermaid
flowchart TD
    p0((Channel.fromPath))
    p1[gitlab_mr_create]
    p2[archive_dicom_tar]
    p4(( ))
    p5[extract_participant]
    p6[sort_dicom]
    p7[map_dicom]
    p8[convert_dicom]
    p9[salzburg_pipeline:discard_head_volumns]
    p10(( ))
    p0 -->|participant_file_ch| p2
    p1 -->|mr_iid| p2
    p2 --> p4
    p0 -->|participant_file_ch| p5
    p1 -->|mr_iid| p5
    p5 -->|dataset| p6
    p1 -->|mr_iid| p6
    p6 -->|dataset| p7
    p1 -->|mr_iid| p7
    p7 -->|dataset| p8
    p1 -->|mr_iid| p8
    p8 -->|dataset| p9
    p1 -->|mr_iid| p9
    p9 -->|dataset| p10
```

## Software dependencies

The workflow execution requires the following software (its further dependencies are not listed):
- Nextflow
- BIDScoin (BIDS conversion)
- dcm2niix (DICOM to NIFTI conversion)
- python-gitlab (as a module for using GitLab API)
- pydeface and FSL FLIRT (for optional defacing of structural images)
- Volumes Discard (for discarding obsolete fMRI volumes)

Installing such a software stack is not straightforward. Therefore, all the software is packaged in a [Docker image](https://gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/containers/-/tree/add-pyhton-gitlab/anc-data-acquisition?ref_type=heads). The image is fully functional but still under development. The image will be eventually moved into this repository.

The workflow is configured to be executed with Apptainer container. Therefore, to execute it locally, install [Nextflow](https://www.nextflow.io/docs/latest/getstarted.html#installation) and [Apptainer](https://apptainer.org/docs/user/latest/quick_start.html#quick-installation). Using [Docker](https://docs.docker.com/desktop/linux/install/debian/) instead of Apptainer requires changing the configuration file.

Additionally, the workflow currently imports [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/) module in its scripts. This module must be available. You can use Python virtual environment to install it:
```bash
python3 -m venv venv
source venv/bin/activate
pip install python-gitlab
```
We're planing to resolve this dependency better. 

## Configuration

The pipeline configuration is stored in [`ANC_data_acquisition.config`](ANC_data_acquisition.config) file. Make sure to set all parameters before executing the workflow. Many of these parameters do not need changing. We recommend not to overwrite the `ANC_data_acquisition.config` file but to copy it and pass the new file with the `-C` option when executing the pipeline.

|Workflow parameter | Description|
|:------------------|:-----------|
|`params.ARCHIVE_PATH` | Absolute path to the directory where the subject DICOM files should be archived. This directory has to exist. The default path is `$launchDir/archive`. |
|`params.LOCATION` | Specify location of the scanner to trigger site-specific pipeline. Currently only `salzburg` is implemented. |
|`params.NUM_VOLUMES_TO_DISCARD`| For `LOCATION='salzburg'` only, specify how many head volumes should be discarded. |
|`params.DEFACING`| Determine whether the defacing should be performed on the data (the value has to be `true` or `false`). |
|`params.BIDSCOIN_MAP_PATH`| Absolute path to BIDScoin map yaml file. The default map file is `$projectDir/assets/bidscoin_map.yaml` and is delivered with this pipeline. |
|`workDir` | Default value for the Nextflow's work directory. It must be absolute path due to Apptainer binding. The default path is `$launchDir/work`. This path can be also while executing the pipeline  using the `-work-dir` option. |

`projectDir` and `launchDir` are Nextflow's variables set at runtime. They point to the pipeline root directory and the directory where the pipeline was executed, respectively.

| GitLab parameter | Description |
|:-----------------|:------------|
|`params.DO_GITLAB` | `true` or `false` to enable and disable GitLab workflow part, respectively.
|`params.GITLAB_NAMESPACE`| [Namespace](https://docs.gitlab.com/ee/user/namespace/) of the GitLab project for storing BIDS data. The GitLab project must exist. The project name is retrieved from the workflow input. |
|`params.PYTHON_GITLAB_CFG`| Path to local [python-gitlab config file](#gitlab-authentication) which is required for GitLab authentication. This file is automatically bound inside the jobs container. |

### Apptainer cache

The Apptainer images are cached by default in `$workDir/singularity/` directory (yes, `singularity` is correct). To change it, set `cacheDir` variable in `profiles.withapptainer.apptainer` context in the pipeline config file.

### GitLab authentication

Pushing the data into a GitLab repository and reporting the pipeline progress in the new data GitLab merge request requires authentication. An access token with scope `api` must be provided. The pipeline uses [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/index.html) for the GitLab API communication. Provide `url` and `private_token` properties in [one of the configuration files looked up by default by python-gitlab](https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-files) in section `[anc-data-acquisition]`, for example:
```
[anc-data-acquisition]
url = https://gitlab.com/
private_token = my-private-access-token
api_version = 4
```

The configuration file is automatically bound inside of the jobs containerto `/etc/python-gitlab.cfg`. This provides authentication for the GitLab actions from inside of the container.

Use [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) if your user should be used for GitLab contributions. Otherwise, use [project](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) or [group access token](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html).

## Usage

In order to start the data acquisition workflow execute the following command (`<path-to-participant_file>` is the path to the TAR file with subject DICOM files that should be converted):
```bash
nextflow -C ANC_data_acquisition.config run ANC_data_acquisition.nf -profile withapptainer --PARTICIPANT_FILE <path-to-participant_file>
```

The `-C` option specifies the pipeline configuration file. If you copied the configuration to another file, change it in the command.

The workflow may generate significant amount of data. Use `-work-dir <path-to-work-dir>` option after the Nextflow script name in the command to specify where the workflow files will be stored.

## Testing

Testing assets are in `test` directory.

### Generate basic input data
Use [DICOM generator](https://gitlab.com/ccns/neurocog/neurodataops/anc/software/bids-miscellaneous-scripts#generate-dicom-files)'s Docker image to generate inputs.

Basic input is specified in [`test/basic.json`](test).

Execute the following from the project root to generate DICOM files:

```
apptainer run --bind $PWD/test:/data/test docker://registry.gitlab.com/ccns/neurocog/neurodataops/anc/software/dicom-generator /data/test/basic.json /data/test/sub-001_ses-1 -t 'MR00000{id}'
TEST_FILE_NAME=acqTest_001_1_$(date +%Y%m%d_%H%M%S).tar.xz
```

Create a tar archive:

```
tar -cJf test/$TEST_FILE_NAME -C test/ sub-001_ses-1
```

### CI/CD

We automatically test the data acquisition workflow with GitLab's CI/CD mechanism. For details, see `.gitlab-ci.yml`. The workflow has a dedicated profile for CI/CD execution (`-profile cicd`). We generate dummy DICOM files, execute the pipeline including MR in a test project, clean up by deleting the branch.
