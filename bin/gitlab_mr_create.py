#!/usr/bin/env python3

import gitlab
import argparse
import sys

parser = argparse.ArgumentParser(description='Create GitLab MR for a new branch.')
parser.add_argument(
    'project_id',
    help = 'GitLab project id for which the MR wll be created'
)
parser.add_argument(
    'branch_name',
    help = 'A branch name for which the MR wll be created'
)
parser.add_argument(
    '--mr-title',
    nargs = 1,
    help = 'MR title; default = branch_name'
)
parser.add_argument(
    '--mr-description',
    nargs = 1,
    help = 'MR description; default is empty'
)
args = parser.parse_args()

gl = gitlab.Gitlab.from_config('anc-data-acquisition')

project = gl.projects.get(args.project_id)

# Create new branch
try:
    branch = project.branches.create({
        'branch': args.branch_name,
        'ref': 'main'
    })
except Exception as e:
    print(e, file=sys.stderr)
    sys.exit(1)

# Create new MR
mr_title = args.branch_name
if args.mr_title is not None:
    mr_title = args.mr_title[0]
mr_description = ''
if args.mr_description is not None:
    mr_description = args.mr_description[0]
try:
    mr = project.mergerequests.create({
        'source_branch': args.branch_name,
        'target_branch': 'main',
        'title': '[Draft] ' + mr_title,
        'description' : mr_description,
        'remove_source_branch' : 'true',
        'squash': 'true',

    })
except Exception as e:
    print(e, file=sys.stderr)
    sys.exit(1)

# Print mr_iid on successful MR creation
print(mr.iid, end='')
