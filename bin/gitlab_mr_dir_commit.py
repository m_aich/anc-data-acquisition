#!/usr/bin/env python3

import gitlab
import argparse
import sys
import base64
from pathlib import Path

parser = argparse.ArgumentParser(description='Add directory and all files in it to GitLab MR branch. Preserve subdirectories.')
parser.add_argument(
    'project_id',
    help = 'GitLab project id for which the MR wll be created'
)
parser.add_argument(
    'branch_name',
    help = 'MR branch to which commit the file'
)
parser.add_argument(
    'dir_path',
    help = 'Path to directory to commit'
)
parser.add_argument(
    'repository_path',
    help = 'Path in the repository under which all files are committed to'
)
parser.add_argument(
    'commit_message',
    help = 'Commit message'
)
parser.add_argument(
    '-b','--binary',
    dest='binary_ext',
    nargs='+',
    help='List of file extensions (final components with a dot) that should be committed as binary',
)

args = parser.parse_args()

gl = gitlab.Gitlab.from_config('anc-data-acquisition')

project = gl.projects.get(args.project_id)

dir_path = Path(args.dir_path)
repository_path = Path(args.repository_path)

data = {
    'branch': args.branch_name,
    'commit_message': args.commit_message,
    'actions': []
}

for path in dir_path.rglob('*.*'):
    commit_path = repository_path / dir_path.parts[-1] / path.relative_to(dir_path)
    print(commit_path)
    print(commit_path.suffix)
    if commit_path.suffix in args.binary_ext:
        action = {
            # Binary files need to be base64 encoded
            'action': 'create',
            'file_path': str(commit_path),
            'content': base64.b64encode(open(path, mode='r+b').read()).decode(),
            'encoding': 'base64',
        }
    else:
        action = {
            # Text files
            'action': 'create',
            'file_path': str(commit_path),
            'content': open(path).read(),
        }
    data['actions'].append(action)

# Create commit
try:
    commit = project.commits.create(data)
except Exception as e:
    print('[ERROR]', e)
    sys.exit(1)
