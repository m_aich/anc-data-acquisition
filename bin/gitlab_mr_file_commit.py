#!/usr/bin/env python3

import gitlab
import argparse
import sys
import base64

parser = argparse.ArgumentParser(description='Add a new file to GitLab MR branch.')
parser.add_argument(
    'project_id',
    help = 'GitLab project id for which the MR wll be created'
)
parser.add_argument(
    'branch_name',
    help = 'MR branch to which commit the file'
)
parser.add_argument(
    'file_path',
    help = 'Path to file to commit'
)
parser.add_argument(
    'repository_path',
    help = 'Path in the repository under which the file is committed to'
)
parser.add_argument(
    'commit_message',
    help = 'Commit message'
)
args = parser.parse_args()

gl = gitlab.Gitlab.from_config('anc-data-acquisition')

project = gl.projects.get(args.project_id)

# Create commit with the file
try:
    data = {
        'branch': args.branch_name,
        'commit_message': args.commit_message,
        'actions': [
            {
                # Binary files need to be base64 encoded
                'action': 'create',
                'file_path': args.repository_path,
                'content': base64.b64encode(open(args.file_path, mode='r+b').read()).decode(),
                'encoding': 'base64',
            }
        ]
    }
    commit = project.commits.create(data)
except Exception as e:
    print('[ERROR]', e)
    sys.exit(1)
