#!/usr/bin/env nextflow

// Declare syntax version
nextflow.enable.dsl=2

// Process participant file name
// Get participant info
participant_file_name_pattern = ~/^([0-9A-Za-z]+)_([0-9A-Za-z]+)_([0-9A-Za-z_]+_){0,1}([0-9]{8})(_[0-9]{6}){0,1}(\.tar)(\.bz2|\.xz)$/

participant_file = file(params.PARTICIPANT_FILE)
participant_file_name = participant_file.getName()

// Error on invalid syntax
assert participant_file_name ==~ participant_file_name_pattern

participant_matcher = participant_file_name =~ participant_file_name_pattern

study = participant_matcher[0][1]
sub = participant_matcher[0][2]
ses = participant_matcher[0][3].replaceFirst(/_/, "")
// #echo "${BASH_REMATCH[4]}" # `DATE`
// #echo "${BASH_REMATCH[5]}" # `_TIME`
// #echo "${BASH_REMATCH[6]}" # `.tar`
compression = participant_matcher[0][7]

branch_name = "sub-" + sub
if (ses.length() > 0) {
    branch_name += "_ses-" + ses
}

project_id = params.GITLAB_NAMESPACE + "/" + study

println("INFO STUDY:\t\t" + study)
println("INFO SUBJECT:\t\t" + sub)
println("INFO SESSION:\t\t" + ses)
println("INFO COMPRESSION:\t" + compression)
println("INFO BRANCH_NAME:\t" + branch_name)
println("INFO PROJECT_ID:\t" + project_id)

// System.exit(0)

//Archieve (copy) incoming tar files to ${params.ARCHIVE_PATH}
process archive_dicom_tar {
    input:
        path dicom_tar
        val mr_iid

    output:
        stdout

    script:
        def command =
            """
            if [ -e ${params.ARCHIVE_PATH} ]
            then
                cp -f $dicom_tar ${params.ARCHIVE_PATH}
                echo "INFO $dicom_tar has been archived in \\"${params.ARCHIVE_PATH}\\"."
            else
                echo "ERROR the path defined in params.ARCHIVE_PATH \\"${params.ARCHIVE_PATH}\\" does not exist."
                exit 1
            fi
            """
        if (params.DO_GITLAB == true)
            command +=
                """
                gitlab_mr_note_create.py \
                    $project_id \
                    $mr_iid \
                    'Subject DICOM raw data is archived.'
                """
        else
            command +=
                """
                echo "INFO GitLab disabled in process $task.process"
                """
        return command
}

//Extract subject's data in ${params.PARTICIPANT_FILE_PATH} tar fole to ../work/*/dataset/sourcedata
process extract_participant {
    input:
        path dicom_tar
        val mr_iid

    output:
        path 'dataset'

    script:
        def command =
            """
            TARGET_DIR="./dataset/sourcedata"

            SUBJECT_DIR=\${TARGET_DIR}/sub-$sub
            if [ ! -z "$ses" ]; then
                SUBJECT_DIR=\$SUBJECT_DIR/ses-$ses
            fi
            readonly SUBJECT_DIR

            # [ ] Try to use `pbzip2` or `lbzip2` for faster decompression
            #     (https://askubuntu.com/questions/214458/any-linux-command-to-perform-parallel-decompression-of-tar-bz2-file)
            #     [ ] Verify if it's available on the host - install in image/add to local dependencies
            COMPRESSION_TOOL=""
            if [ $compression == ".bz2" ]; then
                COMPRESSION_TOOL="--use-compress-program=lbzip2"
            fi
            readonly COMPRESSION_TOOL

            echo "INFO Extracting $dicom_tar into \$SUBJECT_DIR"
            echo "INFO Compression used: $compression"
            echo "INFO Using '\${COMPRESSION_TOOL}' to extract archives"

            mkdir -p \$SUBJECT_DIR
            tar \$COMPRESSION_TOOL -xf $dicom_tar --directory \$SUBJECT_DIR --strip-components=1
            
            # Old process command, when extracting was in external script
            # $projectDir/extract_participant.sh $dicom_tar ./dataset/sourcedata
            """
        if (params.DO_GITLAB == true)
            command +=
                """
                gitlab_mr_note_create.py \
                    $project_id \
                    $mr_iid \
                    'Subject DICOM files are extracted.'
                """
        else
            command +=
                """
                echo "INFO GitLab disabled in process $task.process"
                """
        return command
}

//Sort DICOM files in ./dataset/ with BIDScoin
process sort_dicom {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/containers/anc-data-acquisition:develop'

    input:
        path dataset
        val mr_iid

    output:
        path 'dataset'

    script:
        def command =
            """
            dicomsort $dataset/sourcedata \
            --subprefix sub \
            --sesprefix ses \
            --pattern '.*\\/[A-Z]{2}\\d{6}'
            """
        if (params.DO_GITLAB == true)
            command +=
                """
                readonly DESCRIPTION=\$(ls -1 dataset/sourcedata/sub-$sub/ses-$ses)
                echo \$DESCRIPTION
                gitlab_mr_note_create.py \
                    $project_id \
                    $mr_iid \
                    "DICOM files are sorted. Found the following sequences:\n<pre><code>\${DESCRIPTION}</code></pre>"
                """
        else
            command +=
                """
                echo "INFO GitLab disabled in process $task.process"
                """
        return command
}

//Map DICOM files in ./dataset with BIDScoin
process map_dicom {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/containers/anc-data-acquisition:develop'

    input:
        path dataset
        val mr_iid

    output:
        path 'dataset'

    script:
        def command =
            """
            bidsmapper \
            --template ${params.BIDSCOIN_MAP_PATH} -a \
            $dataset/sourcedata \
            $dataset
            """
        if (params.DO_GITLAB == true)
            command +=
                """
                readonly BIDSCOIN_LOG=\$(< $dataset/code/bidscoin/bidsmapper.log)
                gitlab_mr_note_create.py \
                    $project_id \
                    $mr_iid \
                    "BIDScoin mapping.\n\n<details><summary>Generated log</summary>\n<pre><code>\${BIDSCOIN_LOG}</code></pre>\n\n</details>"
                """
        else
            command +=
                """
                echo "INFO GitLab disabled in process $task.process"
                """
        return command
}

//Converts DICOM files into BIDS format
process convert_dicom {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/containers/anc-data-acquisition:develop'

    input:
        path dataset
        val mr_iid

    output:
        path 'dataset'

    script:
        def command =
            """
            bidscoiner $dataset/sourcedata $dataset
            """
        if (params.DO_GITLAB == true)
            command +=
                """
                readonly BIDSCOIN_LOG=\$(< $dataset/code/bidscoin/bidscoiner.log)
                gitlab_mr_note_create.py \
                    $project_id \
                    $mr_iid \
                    "BIDScoin conversion.\n\n<details><summary>Generated log</summary>\n<pre><code>\${BIDSCOIN_LOG}</code></pre>\n\n</details>"
                """
        else
            command +=
                """
                echo "INFO GitLab disabled in process $task.process"
                """
        return command
}

//Discard head volumns from fMRI NIFTI images
process discard_head_volumns {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/containers/anc-data-acquisition:develop'

    input:
        path dataset
        val mr_iid

    output:
        path 'dataset'

    script:
        def note_body =
            """\
            Discarded \\`${params.NUM_VOLUMES_TO_DISCARD}\\` volumes from functional images.
            
            This information is preserved in \\`*/func/*_bold.json\\` files under they key \\`NumberOfVolumesDiscardedByUser\\`.
            """.stripIndent()
        
        def command =
            """
            volumes-discard $dataset $dataset ${params.NUM_VOLUMES_TO_DISCARD} --retain-paths
            """
        if (params.DO_GITLAB == true)
            command +=
                """
                gitlab_mr_note_create.py \
                    $project_id \
                    $mr_iid \
                    "${note_body}"
                """
        else
            command +=
                """
                echo "INFO GitLab disabled in process $task.process"
                """
        return command
}

// Deface structural images `./dataset/*/*_T?w.nii.gz*`
// Verify if there is only one subject directory and error out otherwise.
process deface {
    container 'registry.gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/containers/anc-data-acquisition:develop'

    input:
        path dataset
        val mr_iid

    output:
        path 'dataset'

    script:
        def command =
            """
            echo "INFO dataset=$dataset"
            find $dataset/ -maxdepth 1 -type d -name 'sub-*'
            echo "INFO PWD=\$PWD"
            echo "INFO ls -al=\$(ls -al)"
            SUB=\$(find $dataset/ -maxdepth 1 -type d -name 'sub-*')
            echo "INFO SUB=\$SUB"
            touch defaced_files.txt
            for f in \$(find \$SUB -type f -name '*_T?w.nii*')
            do
                echo "INFO found \${f}"
                echo "INFO defacing \${f}"
                echo \${f#${dataset}/} >> defaced_files.txt
                pydeface \${f} --outfile \${f} --force --verbose
            done
            """
        if (params.DO_GITLAB == true)
            command +=
                """
                readonly DEFACED_FILES=\$(< defaced_files.txt)
                gitlab_mr_note_create.py \
                    $project_id \
                    $mr_iid \
                    "Defacing in the following files:\n<pre><code>\${DEFACED_FILES}</code></pre>"
                """
        else
            command +=
                """
                echo "INFO GitLab disabled in process $task.process"
                """
        return command
}

//Adds all files from the subject durectory with content to the MR branch
process gitlab_mr_files_commit {
    input:
        path dataset

    """
    echo "INFO Committing $dataset/sub-$sub"
    gitlab_mr_dir_commit.py \
        $project_id \
        $branch_name \
        $dataset/sub-$sub \
        '.' \
        "Adds new data for sub-$sub/ses-$ses" \
        -b .nii
    """
}

process gitlab_mr_create {
    output:
        stdout

    script:
        def mr_body =
            """\
            A new subject data has arrived for processing. See MR notes for any updates.

            |Subject property|Property value|
            |:--|:--|
            |File being processed|\\`$participant_file_name\\`|
            |Study abbreviation|\\`$study\\`|
            |Subject id|\\`$sub\\`|
            |Session id|\\`$ses\\`|
            """.stripIndent()

        def command =
            """
            echo "INFO GitLab disabled in process $task.process"
            """
        if (params.DO_GITLAB == true)
            command = 
                """
                gitlab_mr_create.py \
                    $project_id \
                    $branch_name \
                    --mr-title 'Add data of new subject $branch_name' \
                    --mr-description "${mr_body}"
                """
        return command
}

process linkMyDir {
    
    input:
        path dataset
    
    shell:
	    '''	    

	    mkdir -p ../../../myPublishDir/ #to ensure that there is a publishDir
	    ls -d ./dataset/sub-* |  xargs ln -vsfr -t ../../../myPublishDir/ 
	    '''
	   
    }

workflow {
    participant_file_ch = channel.fromPath(params.PARTICIPANT_FILE)

    def mr_iid_val_ch = gitlab_mr_create()

    // A hack to be able to use MR iid in workflow.onComplete handler
    // Sets a script property with mr_iid value
    mr_iid_val_ch.subscribe { this.setProperty("mr_iid", it) }

    archive_dicom_tar(participant_file_ch, mr_iid_val_ch) | view

    extract_participant(participant_file_ch, mr_iid_val_ch)
    sort_dicom(extract_participant.out, mr_iid_val_ch)
    map_dicom(sort_dicom.out, mr_iid_val_ch)
    convert_dicom(map_dicom.out, mr_iid_val_ch)

    dataset = convert_dicom.out
    if (params.LOCATION == 'salzburg') {
        dataset = salzburg_pipeline(dataset, mr_iid_val_ch)
    }
    if (params.DEFACING == true) {
        dataset = deface(dataset, mr_iid_val_ch)
    }

    if (params.DO_GITLAB == true) {
        gitlab_mr_files_commit(dataset)
    } else {
	    // else publishDir (maybe only the new subject)
	    linkMyDir(dataset)
    }
}

workflow salzburg_pipeline {
    take:
        dataset
        mr_iid
    main:
        discard_head_volumns(dataset, mr_iid)
    emit:
        discard_head_volumns.out
}

workflow.onComplete {
    // println(this.getProperty("mr_iid"))

    // MR note with complete notification
    if (params.DO_GITLAB == true) {
        def note_body =
            """\
            Acquisition workflow completed.

            Execution status: **${ workflow.success ? 'SUCCESS' : 'FAIL' }**
            """.stripIndent()
        def cmd_list = [
            "${baseDir}/bin/gitlab_mr_note_create.py",
                    "${project_id}", 
                    this.getProperty("mr_iid"), 
                    "${note_body}"
            ]
        def proc = cmd_list.execute()
        proc.waitFor()
        // print(proc.err.text)
        // print(proc.in.text)
        // print(proc.exitValue())
    }
    println("INFO Acquisition workflow completed. Execution status: ${ workflow.success ? 'SUCCESS' : 'FAIL' }")
}

workflow.onError {
    // MR note with error notification
    if (params.DO_GITLAB == true) {
        def error_report = workflow.errorReport
        // This replace is necessary for the correct code formatting in MR note
        error_report = error_report.replaceAll("(?m)^", "            ")
        def note_body =
            """\
            An error occured during acquisition workflow. Please contact the administrator with the link to this note.
            
            <details>
            <summary>Workflow error report</summary>

            <pre><code>${error_report}</code></pre>

            </details>
            """.stripIndent()
        def cmd_list = [
            "${baseDir}/bin/gitlab_mr_note_create.py",
                    "${project_id}", 
                    this.getProperty("mr_iid"), 
                    "${note_body}"
            ]
        def proc = cmd_list.execute()
        proc.waitFor()
        // print(proc.err.text)
        // print(proc.in.text)
        // print(proc.exitValue())
    }
    println("ERROR An error occured during acquisition workflow. Execution status: ${ workflow.success ? 'SUCCESS' : 'FAIL' }")
}
